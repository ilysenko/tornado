import tornado.ioloop
import tornado.web
import tornadoredis
from tornado.options import define, parse_command_line, options
import json
from lepl.apps.rfc3696 import Email
import datetime
import iso8601
import motor
import re

email_validator = Email()

define('database', default=1, group='application', help="0-Mogdo DB, 1-Redis")
define('key', default='tornado_test', group='application', help="Default key for test task")
define('connection', default='', group='application',
       help="Database connection string (e.g. mongodb://localhost:27017, redis://localhost:6379)")

API = {
    0: ('MongoDatabase', 'Using MongoDB database'),
    1: ('RedisDatabase', 'Using Redis database')
}


def parse_connection(connection):
    p = '(?:redis.*://)?(?P<host>[^:/ ]+).?(?P<port>[0-9]*).*'
    m = re.search(p, connection)
    return m.group('host'), int(m.group('port'))


class RedisDatabase:
    def __init__(self, connection):
        try:
            host, port = parse_connection((connection)) if connection else ('localhost', 6379)
            self.db = tornadoredis.Client(host=host, port=port)
            self.db.connect()
        except:
            raise Exception('Redis connection error')

    def set(self, data, callback):
        self.db.sadd(options.key, data, callback=lambda x: callback(True) if x else callback(False))

    def get(self, data, callback):
        self.db.smembers(options.key, callback=lambda x: callback(x))

    def delete(self, data, callback):
        self.db.delete(options.key, callback=lambda x: callback(True) if x else callback(False))


class MongoDatabase:
    def __init__(self, connection):
        try:
            self.db = motor.MotorClient(connection or 'mongodb://localhost:27017').open_sync().test
        except:
            raise Exception('MongoDB connection error')

    def set(self, data, callback):
        self.db.test.insert({options.key: data},
                            callback=lambda y, x: callback(False) if x else callback(True))

    def get(self, data, callback):
        self.db.test.find().to_list(callback=lambda m, e: self.get_done(callback, m, e))

    def get_done(self, callback, messages, error):
        if error:
            callback(False)
        callback([m[options.key] for m in messages])

    def delete(self, data, callback):
        self.db.test.remove(callback=lambda y, x: callback(False) if x else callback(True))


class BaseHandler(tornado.web.RequestHandler):
    def prepare(self):
        self.parse_json()
        self.db = self.settings['db']
        self.set_header("Content-Type", "application/json")

    def result_response(self, res):
        if not res:
            self.write(json.dumps({'success': False}))
        elif res == True:
            self.write(json.dumps({'success': True}))
        else:
            self.write(json.dumps(list(res)))
        self.finish()

    def parse_json(self):
        try:
            self.json_data = json.loads(self.request.body)
        except ValueError:
            raise tornado.web.HTTPError(404)
        if not ('email' in self.json_data and 'data' in self.json_data and 'date' in self.json_data):
            raise tornado.web.HTTPError(404)
        if not email_validator(self.json_data['email']):
            raise tornado.web.HTTPError(404)
        try:
            date = iso8601.parse_date(self.json_data['date'])
        except:
            raise tornado.web.HTTPError(404)
        if (date.replace(tzinfo=None) - datetime.datetime.utcnow()).days > 9:
            raise tornado.web.HTTPError(403)


class Set(BaseHandler):
    @tornado.web.asynchronous
    def post(self):
        self.db.set(self.json_data['data'], self.result_response)


class Get(BaseHandler):
    @tornado.web.asynchronous
    def post(self):
        self.db.get(self.json_data['data'], self.result_response)


class Del(BaseHandler):
    @tornado.web.asynchronous
    def post(self):
        self.db.delete(self.json_data['data'], self.result_response)


def application(db):
    return tornado.web.Application([
                                       (r"/set/", Set),
                                       (r"/get/", Get),
                                       (r"/del/", Del),
                                   ], db=db, debug=True)


if __name__ == "__main__":
    tornado.options.parse_command_line()
    print API[options.database][1]
    try:
        db = globals()[API[options.database][0]](options.connection)
        application(db).listen(8888)
        tornado.ioloop.IOLoop.instance().start()
    except Exception, e:
        print str(e)