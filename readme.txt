git clone git@bitbucket.org:ilysenko/tornado.git
cd tornado
virtualenv ENV --no-site-packages
source ENV/bin/activate
pip install -r requirements.txt
python main.py --help


usage:

http POST 127.0.0.1:8888/set/ < a.json
http POST 127.0.0.1:8888/get/ < a.json
http POST 127.0.0.1:8888/del/ < a.json