from tornado.ioloop import IOLoop
from tornado.options import options
import unittest
from main import API, application, RedisDatabase, MongoDatabase
from tornado.testing import AsyncHTTPTestCase
import json

valid_json = '{"email": "test@gmail.com", "date": "2013-08-21T12:00:54Z", "data": {"random": "object3"}}'
invalid_json = '"email": "test@gmail.com", "date": "2013-08-21T12:00:54Z", "data": {"random": "object3"}}'
invalid_date = '{"email": "test@gmail.com", "date": "13-08-21T12:00:54Z", "data": {"random": "object3"}}'
invalid_email = '{"email": "test@@gmail.com", "date": "2013-08-21T12:00:54Z", "data": {"random": "object3"}}'
future_date = '{"email": "test@gmail.com", "date": "2020-08-21T12:00:54Z", "data": {"random": "object3"}}'


class AsyncAppTestCaseRedis(AsyncHTTPTestCase):
    db = globals()[API[1][0]](options.connection)

    def get_new_ioloop(self):
        return IOLoop.instance()

    def get_app(self):
        return application(self.db)

    def shortcut(self, url, data, code=None, substr=None):
        self.http_client.fetch(self.get_url(url), self.stop, method="POST", body=data)
        response = self.wait()
        if code:
            assert response.code == code
        elif substr:
            assert substr in response.body
        else:
            return response

    def test_valid_json_response(self):
        self.shortcut('/get/', valid_json, code=200)

    def test_invalid_json_response(self):
        self.shortcut('/get/', invalid_json, code=404)

    def test_invalid_date(self):
        self.shortcut('/get/', invalid_date, code=404)

    def test_date_10_day_difference(self):
        self.shortcut('/get/', future_date, code=403)

    def test_invalid_email(self):
        self.shortcut('/get/', invalid_email, code=404)

    def test_set(self):
        self.shortcut('/set/', valid_json, substr='success')

    def test_get(self):
        assert len(json.loads(self.shortcut('/get/', valid_json).body)) > 0

    def test_del(self):
        self.shortcut('/del/', valid_json, substr='success')

    def test_get_after_del(self):
        self.shortcut('/get/', valid_json, substr='success')


class AsyncAppTestCaseMongoDB(AsyncAppTestCaseRedis):
    db = globals()[API[0][0]](options.connection)


if __name__ == '__main__':
    unittest.main()